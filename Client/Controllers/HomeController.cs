﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Client.Models;
using System.Threading.Tasks;
using NCalc;
using System.Configuration;
using Client.Utils;
using System.Net;
using System.Runtime.Serialization.Json;
using System.ServiceModel.Web;
using System.Net.Http;

namespace Client.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(EquationViewModel eq)
        {

            if(eq.IsDistributed)
            {
                List<Task<EquationResultViewModel>> nodeTaskList = new List<Task<EquationResultViewModel>>();

                var nodes = ((NodesConfigSection)ConfigurationManager.GetSection("nodesConfig")).Nodes.Cast<NodeElement>().Where(x => x.IsActive).ToArray();

                List<NodeElement> availableNodes = new List<NodeElement>();

                //sprawdź dostępność nóg
                foreach (var n in nodes)
                {
                    using (var http = new HttpClient())
                    {
                        Task<HttpResponseMessage> t = null;
                        try
                        {
                            t = http.GetAsync(n.Host.Replace("values", "ping"));
                            Task.WaitAll(t);

                            if (t.Result.IsSuccessStatusCode)
                                availableNodes.Add(n);

                            t.Dispose();
                        }
                        catch(Exception ex)
                        {
                            if (t != null)
                                t.Dispose();
                        }
                    }
                }

                nodes = availableNodes.ToArray();


                int nodesCount = nodes.Count();

                double nodeSectionSize = (eq.RightBorder - eq.LeftBorder) / nodesCount;

                for (int i = 0; i < nodesCount; i++)
                {
                    double p1 = eq.LeftBorder + i * nodeSectionSize;
                    double p2 = eq.LeftBorder + (i + 1) * nodeSectionSize;
                    int index = i;
                    nodeTaskList.Add(Task.Factory.StartNew(() => NodeRequest(nodes[index].Host, eq.Equation, p1, p2, eq.PartsNumber)));
                }

                Task.WaitAll(nodeTaskList.ToArray());

                List<double> summaryLabels = new List<double>();
                List<double> summaryValues = new List<double>();
                double sum = 0;

                foreach (var t in nodeTaskList)
                {
                    sum += t.Result.Result;
                    if (summaryLabels.Count > 0)
                        summaryLabels.AddRange(t.Result.Labels.Skip(1));
                    else
                        summaryLabels.AddRange(t.Result.Labels);

                    if (summaryValues.Count > 0)
                        summaryValues.AddRange(t.Result.Values.Skip(1));
                    else
                        summaryValues.AddRange(t.Result.Values);
                }

                EquationResultViewModel vm = new EquationResultViewModel
                {
                    Result = sum,
                    Labels = summaryLabels.ToArray(),
                    Values = summaryValues.ToArray()
                };

                Session["equationResults"] = vm;
            }
            else
            {
                List<Task<double>> taskList = new List<Task<double>>();
                List<double> labels = new List<double>();
                List<double> values = new List<double>();
                Expression e = new Expression(eq.Equation);

                double stepSize = (eq.RightBorder - eq.LeftBorder) / eq.PartsNumber;

                for (int i = 0; i < eq.PartsNumber; i++)
                {
                    double p1 = eq.LeftBorder + i * stepSize;
                    labels.Add(p1);
                    e.Parameters["x"] = p1;
                    values.Add((double)e.Evaluate());
                    double p2 = eq.LeftBorder + (i + 1) * stepSize;

                    taskList.Add(Task.Factory.StartNew(() => CalculateIntegrationPart(eq.Equation, p1, p2)));
                }

                e.Parameters["x"] = (double)eq.RightBorder;
                labels.Add(eq.RightBorder);
                values.Add((double)e.Evaluate());

                Task.WaitAll(taskList.ToArray());

                EquationResultViewModel vm = new EquationResultViewModel
                {
                    Result = taskList.Sum(x => x.Result),
                    Labels = labels.ToArray(),
                    Values = values.ToArray()
                };

                Session["equationResults"] = vm;
            }

            return RedirectToAction("Results");
        }

        public ActionResult Results()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetIntegrationResult()
        {
            EquationResultViewModel vm = Session["equationResults"] as EquationResultViewModel;
            //EquationResultViewModel vm = new EquationResultViewModel
            //{
            //    Result = 5
            //};
            return Json(vm, JsonRequestBehavior.AllowGet);
        }

        private double CalculateIntegrationPart(string equation, double p1, double p2)
        {
            Expression e = new Expression(equation);
            double stepSize = p2 - p1;

            e.Parameters["x"] = p1;
            double resultA = (double)e.Evaluate();

            e.Parameters["x"] = p2;
            double resultB = (double)e.Evaluate();

            return (resultA + resultB) * stepSize / 2;
        }

        private EquationResultViewModel NodeRequest(string host, string equation, double p1, double p2, double steps)
        {
            WebClient wc = new WebClient();
            wc.Headers.Add(HttpRequestHeader.Accept, "json");
            wc.QueryString.Add("Equation", equation);
            wc.QueryString.Add("LeftBorder", string.Format("{0}",p1));
            wc.QueryString.Add("RightBorder", string.Format("{0}", p2));
            wc.QueryString.Add("PartsNumber", string.Format("{0}", steps));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(EquationResultViewModel));
            return ser.ReadObject(wc.OpenRead(host)) as EquationResultViewModel;
        }

    }
}