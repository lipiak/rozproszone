﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Client.Utils
{
    public class NodesConfigSection : ConfigurationSection
    {
        //node element
        [ConfigurationProperty("nodes", IsDefaultCollection = true)]
        public NodesCollection Nodes
        {
            get
            {
                return (NodesCollection)base["nodes"];
            }
        }
    }

    public class NodesCollection : ConfigurationElementCollection
    {
        public new NodeElement this[string name]
        {
            get
            {
                if (IndexOf(name) < 0) return null;

                return (NodeElement)BaseGet(name);
            }
        }

        public NodeElement this[int index]
        {
            get { return (NodeElement)BaseGet(index); }
        }

        public int IndexOf(string name)
        {
            name = name.ToLower();

            for (int idx = 0; idx < base.Count; idx++)
            {
                if (this[idx].Host.ToLower() == name)
                    return idx;
            }
            return -1;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new NodeElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((NodeElement)element).Host;
        }

        protected override string ElementName
        {
            get { return "node"; }
        }
    }

    public class NodeElement : ConfigurationElement
    {
        [ConfigurationProperty("host")]
        public string Host
        {
            get { return (string)this["host"]; }
        }

        [ConfigurationProperty("active")]
        public bool IsActive
        {
            get { return (bool)this["active"]; }
        }
    }
}