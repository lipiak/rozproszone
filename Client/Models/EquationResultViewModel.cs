﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Client.Models
{
    public class EquationResultViewModel
    {
        public double Result { get; set; }
        public double[] Labels { get; set; }
        public double[] Values { get; set; }
    }
}