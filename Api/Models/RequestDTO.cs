﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public class RequestDTO
    {
        private string _equation;
        public string Equation
        {
            get { return _equation; }
            set { _equation = value; }
        }

        private float _leftBorder;
        public float LeftBorder
        {
            get { return _leftBorder; }
            set { _leftBorder = value; }
        }

        private float _rightBorder;
        public float RightBorder
        {
            get { return _rightBorder; }
            set { _rightBorder = value; }
        }

        private int _partsNumber;
        public int PartsNumber
        {
            get { return _partsNumber; }
            set { _partsNumber = value; }
        }
    }
}