﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public class ResponseDTO
    {
        public double Result { get; set; }
        public double[] Labels { get; set; }
        public double[] Values { get; set; }
    }
}