﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Api.Models;
using System.Threading.Tasks;
using NCalc;

namespace Api.Controllers
{
    public class ValuesController : ApiController
    {
        public ResponseDTO Get([FromUri]RequestDTO data)
        {

            try
            {

                List<Task<double>> taskList = new List<Task<double>>();
                List<double> labels = new List<double>();
                List<double> values = new List<double>();
                Expression e = new Expression(data.Equation);

                double stepSize = (data.RightBorder - data.LeftBorder) / data.PartsNumber;

                for (int i = 0; i < data.PartsNumber; i++)
                {
                    double p1 = data.LeftBorder + i * stepSize;
                    double p2 = data.LeftBorder + (i + 1) * stepSize;

                    taskList.Add(Task.Factory.StartNew<double>(() => Calculate(data.Equation, p1, p2, stepSize)));

                    labels.Add(p1);
                    e.Parameters["x"] = p1;
                    values.Add((double)e.Evaluate());
                }

                labels.Add(data.RightBorder);
                e.Parameters["x"] = (double)data.RightBorder;
                values.Add((double)e.Evaluate());

                Task.WaitAll(taskList.ToArray());

                return new ResponseDTO
                {
                    Labels = labels.ToArray(),
                    Values = values.ToArray(),
                    Result = taskList.Sum(x => x.Result)
                };
            }
            catch (Exception ex)
            {
                return new ResponseDTO()
                {
                    Labels = new double[0],
                    Values = new double[0],
                    Result = 0
                };
            }
        }

        private double Calculate(string expression, double p1, double p2, double stepSize)
        {
            Expression e = new Expression(expression);
            e.Parameters["x"] = p1;
            double resultA = (double)e.Evaluate();
            e.Parameters["x"] = p2;
            double resultB = (double)e.Evaluate();
            return (resultA + resultB) * stepSize / 2;
        }
    }
}
